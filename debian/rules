#!/usr/bin/make -f

# This is to turn on verbose mode.
# export DH_VERBOSE=1

# Based on rules file from rt-extension-calendar

RT_VERSIONS=4
BASE=extension-repeatticket

%:
	dh $@

override_dh_auto_configure:
	mkdir -p bak
	mv inc bak
	for f in META.yml; do cp -pr $$f bak; done

	rm -rf inc/Module
	for ver in $(RT_VERSIONS); do \
	  RTHOME=/usr/share/request-tracker$$ver INSTALLDIRS=vendor PREFIX= perl Makefile.PL INSTALLDIRS=vendor; \
	  mv Makefile Makefile$$ver; \
	done

override_dh_auto_build-indep:
	pod2man bin/rt-repeat-ticket > bin/rt-repeat-ticket.1
	dh_auto_build

# The use of a common package is not currently used, the man page handling is
# left for future reference if we need to build for multiple versions again.
override_dh_auto_install:
	for ver in $(RT_VERSIONS); do \
	  ln -sf Makefile$$ver Makefile; \
	  export PERL_INSTALL_ROOT=$(CURDIR)/debian/rt$$ver-$(BASE); \
	  make install; \
	  find debian/rt$$ver-$(BASE) -type f -name "*.in" -delete; \
	  mv -f debian/rt$$ver-$(BASE)/usr/share/man/man3/RT::Extension::RepeatTicket.3pm \
	    debian/rt$$ver-$(BASE)/usr/share/man/man3/RT::Extension::RepeatTicket-$$ver.3pm; \
	  mkdir -p debian/rt$$ver-$(BASE)/usr/bin; \
	  mv -f debian/rt$$ver-$(BASE)/usr/share/request-tracker$$ver/plugins/RT-Extension-RepeatTicket/bin/* \
	    debian/rt$$ver-$(BASE)/usr/bin; \
	  rmdir debian/rt$$ver-$(BASE)/usr/share/request-tracker$$ver/plugins/RT-Extension-RepeatTicket/bin; \
	  mkdir debian/rt$$ver-$(BASE)/usr/share/man/man1; \
	  mv -f debian/rt$$ver-$(BASE)/usr/bin/rt-repeat-ticket.1 \
	    debian/rt$$ver-$(BASE)/usr/share/man/man1/rt-repeat-ticket-$$ver.1; \
	  for file in debian/rt$$ver-$(BASE)/usr/bin/*; do \
	    mv $$file $$file-$$ver; \
	  done; \
	done

override_dh_auto_clean:
	dh_auto_clean

	# Restore shipped generated files.
	find bak -type f -print | sed -e 's/^bak\///' | while read f; do mkdir -p $$(dirname $$f); mv bak/$$f $$f; done
	rm -rf bak

	# Delete generated files.
	rm -f bin/rt-repeat-ticket bin/rt-repeat-ticket.1 lib/RT/Extension/RepeatTicket/Test.pm

	for ver in $(RT_VERSIONS); do \
	  rm -f Makefile$$ver; \
	done
